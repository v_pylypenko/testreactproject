import React, { Component } from 'react';

export default class AddItem extends Component{

    state = {
        label:''
    };

    onChange = (event)=>{
        this.setState({label: event.target.value})
    };

    onSubmit = (event) =>{
        event.preventDefault();
        this.props.onItemAdd(this.state.label);
        this.setState({
            label: ''
        });
    };

    render(){
        return(
            <form className="top-panel d-flex" onSubmit={this.onSubmit}>
                <input required type="text" className="form-control search-input"
                       onChange={this.onChange} placeholder="What's new?"
                        value={this.state.label}/>
                <button type="submit" className="btn btn-outline-dark">
                    Add Item
                </button>
            </form>
        );
    }
}


