import React, {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import ToDoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import AddItem from '../add-form';
import './app.css';

export default class App extends Component{

    ID = 1;

    createItem = (label) => {
        return{
            label: label,
            important: false,
            done: false,
            id:++this.ID
        }
    };

    state = {
        todoData: [
            this.createItem('Go to the gym'),
            this.createItem('To congratulate Dimas'),
            this.createItem('To ski')
        ],
        term:'',
        filter:'all'
    };

    deleteItem = (id)=>{
        this.setState(({todoData})=>{
            const index = todoData.findIndex((el)=> el.id === id);

            const newArr = [...todoData.slice(0,index), ...todoData.slice(index+1)];
            return{
                todoData: newArr
            };
        });
    };

    addItem = (text)=>{
        const newItem = this.createItem(text);
        this.setState(({todoData})=>{
            const newArr = [...todoData,newItem];
            return{
                todoData:newArr
            }
        })
    };

    onSearchChange = (term) => {
        this.setState({term});
    };

    onFilterChange = (filter) => {
        this.setState({filter});
    };

    search = (items, term) =>{
        if (term ===0 )
            return items;
         return items.filter((item) => {
            return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1;
        })
    };

    filter = (items, filter) => {
        switch (filter) {
            case 'all':
                return items;
            case 'active':
                return items.filter((item) => !item.done);
            case 'done':
                return items.filter((item) => item.done);
            default:
                return items;
        }
    };

    toggleProp = (arr, id, propName) => {
        const index = arr.findIndex((el) => el.id === id);

        const oldItem = arr[index];
        const newItem = {...oldItem, [propName]: !oldItem[propName]};
        return [...arr.slice(0, index), newItem, ...arr.slice(index + 1)];
    };

    onToggleDone = (id) =>{
        this.setState(({todoData})=>{
            return{
                todoData: this.toggleProp(todoData,id,'done')
            };
        });
    };

    onToggleImportant = (id)=>{
        this.setState(({todoData})=>{
            return{
                todoData: this.toggleProp(todoData,id,'important')
            };
        });
    };

    render() {
        const{todoData, term, filter} = this.state;
        const visibleItems = this.filter(this.search(todoData,term), filter);
        const doneCount = todoData.filter((el) => el.done).length;
        const todoCount = todoData.length -doneCount;

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel onTermChange={this.onSearchChange}/>
                    <ItemStatusFilter filter={filter} onFilterChange={this.onFilterChange}/>
                </div>
                <ToDoList todo={visibleItems}
                          onDeleted={this.deleteItem}
                          onToggleImportant={this.onToggleImportant}
                          onToggleDone={this.onToggleDone}
                />
                <AddItem onItemAdd={this.addItem}/>
            </div>
        );
    }
}
